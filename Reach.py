class Reach(object):
    def __init__(self, polyline, classification):
        """
        :type polyline: The complete polyline
        """
        self.polyline = polyline
        self.classification = classification